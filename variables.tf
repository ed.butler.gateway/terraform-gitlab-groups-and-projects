variable "parent_group_id" {
  type        = number
  description = "Parent ID for top level group in which all resources are applied within"
}

variable "groups" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id of a parent group"
}

variable "groups_depth_1" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id of a parent group in groups variable"
}

variable "groups_depth_2" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id or path of a parent group in nested_groups"
}

variable "groups_depth_3" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id or path of a parent group in nested_groups"
}

variable "groups_depth_4" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id or path of a parent group in nested_groups"
}

variable "groups_depth_5" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id or path of a parent group in nested_groups"
}

variable "groups_depth_6" {
  type = list(object({
    name         = string
    parent_group = string
  }))
  description = "A list of groups, name is the group name and parent group is the id or path of a parent group in nested_groups"
}

variable "projects" {
  type = list(object({
    name                                  = string
    path                                  = string
    namespace_id                          = string
    import_url                            = string
    default_branch                        = string
    pipelines_enabled                     = bool
    wiki_enabled                          = bool
    merge_requests_enabled                = bool
    mirror                                = bool
    mirror_trigger_builds                 = bool
    approvals_before_merge                = number
    visibility_level                      = string
    only_allow_merge_if_pipeline_succeeds = bool
    type                                  = string
  }))
  description = "A list of projects, name is project name and group is used to perform a lookup on the group name"
}


variable "gitlab_token" {
  type        = string
  description = "access token for gitlab"
  sensitive   = true
}