terraform {
  required_providers {
    gitlab = {
      source  = "eddb7/gitlab"
      version = "3.6.10"
    }
  }
}

terraform {
  backend "azurerm" {}
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_group" "parent" {
  group_id = var.parent_group_id
}

resource "gitlab_group" "groups" {
  count     = length(var.groups)
  name      = var.groups[count.index].name
  path      = var.groups[count.index].name
  parent_id = data.gitlab_group.parent.id
  # parent_id = var.groups[count.index].parent_group != "" ? var.groups[count.index].parent_group : data.gitlab_group.devd.id
}

resource "gitlab_group" "depth1" {
  count     = length(var.groups_depth_1)
  name      = var.groups_depth_1[count.index].name
  path      = var.groups_depth_1[count.index].name
  parent_id = gitlab_group.groups[index(gitlab_group.groups.*.full_path, var.groups_depth_1[count.index].parent_group)].id
}

resource "gitlab_group" "depth2" {
  count     = length(var.groups_depth_2)
  name      = var.groups_depth_2[count.index].name
  path      = var.groups_depth_2[count.index].name
  parent_id = gitlab_group.depth1[index(gitlab_group.depth1.*.full_path, var.groups_depth_2[count.index].parent_group)].id

}

resource "gitlab_group" "depth3" {
  count     = length(var.groups_depth_3)
  name      = var.groups_depth_3[count.index].name
  path      = var.groups_depth_3[count.index].name
  parent_id = gitlab_group.depth2[index(gitlab_group.depth2.*.full_path, var.groups_depth_3[count.index].parent_group)].id

}

resource "gitlab_group" "depth4" {
  count     = length(var.groups_depth_4)
  name      = var.groups_depth_4[count.index].name
  path      = var.groups_depth_4[count.index].name
  parent_id = gitlab_group.depth3[index(gitlab_group.depth3.*.full_path, var.groups_depth_4[count.index].parent_group)].id

}

resource "gitlab_group" "depth5" {
  count     = length(var.groups_depth_5)
  name      = var.groups_depth_5[count.index].name
  path      = var.groups_depth_5[count.index].name
  parent_id = gitlab_group.depth4[index(gitlab_group.depth4.*.full_path, var.groups_depth_5[count.index].parent_group)].id

}

resource "gitlab_group" "depth6" {
  count     = length(var.groups_depth_6)
  name      = var.groups_depth_6[count.index].name
  path      = var.groups_depth_6[count.index].name
  parent_id = gitlab_group.depth5[index(gitlab_group.depth5.*.full_path, var.groups_depth_6[count.index].parent_group)].id

}

resource "gitlab_project" "projects" {
  count                                 = length(var.projects)
  name                                  = var.projects[count.index].name
  path                                  = var.projects[count.index].path != "" ? var.projects[count.index].path : var.projects[count.index].name
  namespace_id                          = var.projects[count.index].namespace_id != "" ? var.projects[count.index].namespace_id : var.parent_group_id
  import_url                            = var.projects[count.index].import_url != "" ? "https://gitlab-ci-token:${var.gitlab_token}@${var.projects[count.index].import_url}" : ""
  default_branch                        = var.projects[count.index].default_branch != "" ? var.projects[count.index].default_branch : "master"
  wiki_enabled                          = var.projects[count.index].wiki_enabled
  pipelines_enabled                     = var.projects[count.index].pipelines_enabled
  merge_requests_enabled                = var.projects[count.index].merge_requests_enabled
  mirror                                = var.projects[count.index].mirror
  mirror_trigger_builds                 = var.projects[count.index].mirror_trigger_builds
  approvals_before_merge                = var.projects[count.index].approvals_before_merge
  visibility_level                      = var.projects[count.index].visibility_level != "" ? var.projects[count.index].visibility_level : "private"
  only_allow_merge_if_pipeline_succeeds = var.projects[count.index].only_allow_merge_if_pipeline_succeeds

  depends_on = [
    gitlab_group.depth6,
    gitlab_group.depth5,
    gitlab_group.depth4,
    gitlab_group.depth3,
    gitlab_group.depth2,
    gitlab_group.depth1,
    gitlab_group.groups
  ]
}

locals {
  # Ids for multiple sets of EC2 instances, merged together
  gitflow_projects  = [for x in var.projects : x if x.type == "gitflow"]
  tbd_projects      = [for x in var.projects : x if x.type == "tbd"]
  tbd2_projects     = [for x in var.projects : x if x.type == "tbd2"]
}


# issue when default branch is switched to main 
resource "gitlab_branch" "develop" {
  count   = length(local.gitflow_projects)
  name    = "develop"
  ref     = "master"
  project = gitlab_project.projects[index(gitlab_project.projects.*.name, local.gitflow_projects[count.index].name)].id
}

resource "gitlab_branch_protection" "gitflowMaster" {
  count              = length(local.gitflow_projects)
  project            = gitlab_project.projects[index(gitlab_project.projects.*.name, local.gitflow_projects[count.index].name)].id
  branch             = "master"
  push_access_level  = "no one"
  merge_access_level = "developer"
  depends_on = [
    gitlab_project.projects,
  ]
}

resource "gitlab_branch_protection" "gitflowDevelop" {
  count              = length(local.gitflow_projects)
  project            = gitlab_project.projects[index(gitlab_project.projects.*.name, local.gitflow_projects[count.index].name)].id
  branch             = "develop"
  push_access_level  = "no one"
  merge_access_level = "developer"
  depends_on = [
    gitlab_project.projects,
    gitlab_branch.develop
  ]
}

resource "gitlab_branch_protection" "tbdMaster" {
  count              = length(local.tbd_projects)
  project            = gitlab_project.projects[index(gitlab_project.projects.*.name, local.tbd_projects[count.index].name)].id
  branch             = "master"
  push_access_level  = "no one"
  merge_access_level = "developer"
  depends_on = [
    gitlab_project.projects
  ]
}

resource "gitlab_branch_protection" "tbd2Master" {
  count              = length(local.tbd2_projects)
  project            = gitlab_project.projects[index(gitlab_project.projects.*.name, local.tbd2_projects[count.index].name)].id
  branch             = "master"
  push_access_level  = "developer"
  merge_access_level = "developer"
  depends_on = [
    gitlab_project.projects
  ]
}
