output "projects" {
  value       = gitlab_project.projects.*.path_with_namespace
  description = "Created projects"
}